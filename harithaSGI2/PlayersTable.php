<?php
class PlayersTable {
    private $conn;
    
    public function __construct(PDO $conn) {
        $this->conn = $conn;
        $this->create();
    }
    
    public function __destruct() {
        $this->drop();
    }
    
    private function create(){
        $sql = "CREATE TABLE IF NOT EXISTS Players (
            id INT unsigned AUTO_INCREMENT PRIMARY KEY,
            pwd_hash_and_salt VARCHAR(255) NOT NULL,
            name VARCHAR(50),
            credits INT unsigned,
            lifetime_spins INT unsigned
            )";
        try {
            $this->conn->exec($sql);
        }catch(PDOException $e){
           echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    private function drop(){
        $sql = 'DROP TABLE IF EXISTS `Players`';
        try {
            $this->conn->exec($sql);
        }catch(PDOException $e){
          echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }

    public function populate($row_count) {
        try {
            $stmt = $this->conn->prepare("INSERT INTO Players (pwd_hash_and_salt, name, credits, lifetime_spins)
            VALUES (:pwd_hash_and_salt, :name, :credits, :lifetime_spins)");
            $stmt->bindParam(':pwd_hash_and_salt', $pwd_hash_and_salt);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':credits', $credits);
            $stmt->bindParam(':lifetime_spins', $lifetime_spins);

            for ($row = 0; $row < $row_count; $row++)
            {
                $name = "Name".($row+1); // For now, name is auto-incremented string
                $pwd_hash_and_salt = password_hash($name, PASSWORD_DEFAULT); // For now, name is the pwd (for testing). Also hash value includes auto-generated salt.
                $credits = mt_rand(0, 10000); // Each player's credit is a random number between 0 and 10000
                $lifetime_spins = mt_rand(0, 1000); // Each player's lifetime-spin count is a random number between 0 and 1000
                $stmt->execute();
            }
            echo "Players data loaded successfully...\n";
        }catch(PDOException $e){
            echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }

    public function view() {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM Players");
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($results as $result){
                echo $result['id']."|".$result['pwd_hash_and_salt']."|".$result['name']."|".$result['credits']."|".$result['lifetime_spins']."|"."\n";
            }
        }
        catch(PDOException $e) {
            echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    public function update($id, $pwd, $coins_bet, $coins_won) {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM Players WHERE id=$id");
            $stmt->execute();
            if ($stmt->rowCount() <= 0) {
                echo "User id not found!!!"."\n";
                return;
            }
            
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            // Assuming just one record would return.
            $pwd_hash_and_salt = $results[0]['pwd_hash_and_salt'];
            
            if (!password_verify($pwd, $pwd_hash_and_salt)) {
                echo "Invalid password!!!"."\n";
                return;
            }
            
            $name = $results[0]['name'];
            $lifetime_spins = $results[0]['lifetime_spins'];
            
            // Assuming 1 credit value = 1 coin value.
            $credits = $results[0]['credits'];
            $new_credits = $credits - intval($coins_bet) + intval($coins_won);
            $new_lifetime_spins = $lifetime_spins+1; // Increment 1
            
            $sql = "UPDATE Players SET credits=$new_credits, lifetime_spins=$new_lifetime_spins WHERE id=$id";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            
            // Output JSON response...
            $this->ouputJSON($id);
        }
        catch(PDOException $e) {
            echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    private function ouputJSON($id) {
        $stmt = $this->conn->prepare("SELECT * FROM Players WHERE id=$id");
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arr = array(
            "Player ID" => intval($results[0]['id']),
            "Name" => $results[0]['name'],
            "Credits" => intval($results[0]['credits']),
            "Lifetime Spins" => intval($results[0]['lifetime_spins']),
            "Lifetime Average Return" => round($results[0]['credits']/$results[0]['lifetime_spins'], 4, PHP_ROUND_HALF_UP)
            );
        echo json_encode($arr)."\n";
    }
}
?>