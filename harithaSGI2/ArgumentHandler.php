<?php
class ArgumentHandler {
    private $args = array();

    public function __construct($arg_str) {
        foreach (split(' ', trim($arg_str)) as $n)
            $this->args[] = $n;
    }
    
    public function __destruct() {
        $args = null;
    }

    public function hasValidArgs() {
        $arg_count = sizeof($this->args);
        if ($arg_count < 1){
            echo "ERROR: Invalid number of arguments!\n";
            return false;
        }

        switch ($this->args[0]) {
             case "Update":
                if ($arg_count == 5)
                {
                    if (!is_numeric($this->args[1])) {
                        echo "Players' id must be numeric!!!"."\n";
                        return false;
                    }
                    
                    if (!is_numeric($this->args[3]) || !is_numeric($this->args[4])) {
                        echo "Coin value must be numeric!!!"."\n";
                        return false;
                    }
                    
                    if (intval($this->args[3]) <= 0) {
                        echo "Coins bet must be greater than zero!!!"."\n";
                        return false;
                    }
                    
                    if (intval($this->args[4]) < 0) {
                        echo "Coins won cannot be negative!!!"."\n";
                        return false;
                    }
                    
                    return true;
                }
                echo "ERROR: Invalid number of arguments for Update command!\n";
                break;
            case "Help":
            case "Exit":
            case "View":
                return true;
                break;
            default:
                echo "ERROR: Unrecognized command!\n";
                break;
        }
        
        return false;
    }

    public function isExit() {
        if (!$this->hasValidArgs())
            return false;
            
        return $this->args[0] == "Exit";
    }

    public function runCommand($table) {
        if (!$this->hasValidArgs())
            return;

        switch ($this->args[0]) {
            case "Update":
                $table->update($this->args[1], $this->args[2], $this->args[3], $this->args[4]);
                break;
            case "View":
                $table->view();
                break;
            case "Help":
                echo "Enter Update command as: [Update <id> <password> <no.of coins bet> <no.of coins won>]. [View|Help|Exit] command has no arguments.\n";
                break;
            default:
                echo "ERROR: Unrecognized command type in the arguments!\n";
                break;
        }
    }
}
?>