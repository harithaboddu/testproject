<?php
include('DatabaseConnection.php');
include('PlayersTable.php');
include('ArgumentHandler.php');

$db_conn = new DatabaseConnection();
$conn = $db_conn->getConnection();

$table = new PlayersTable($conn);
$num_records_to_populate = 10;
$table->populate($num_records_to_populate);

while (1) {
    fwrite(STDOUT, "Enter [Update|View|Help|Exit] command: ");
    $arg_hdlr = new ArgumentHandler(fgets(STDIN));
    
    if ($arg_hdlr->hasValidArgs()) {
        if ($arg_hdlr->isExit())
            break;

        $arg_hdlr->runCommand($table);
    }
    
    $arg_hdlr = null;
};

$table = null;
$db_conn = null;
?>
