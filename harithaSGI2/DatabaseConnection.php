<?php
class DatabaseConnection {
    private $conn;
    
    public function __construct() {
        $driver = 'mysql';
        $host = 'host=localhost';
        $database = "dbname=CODINGGROUND";
        $socket = "unix_socket=/home/cg/mysql/mysql.sock";
        $username = 'root';
        $password = 'root';
        
        $dsn = "$driver:$host;$socket;$database";
        try {
           $this->conn = new PDO($dsn, $username, $password);
           $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           echo "Database connection is established...\n";
        }catch(PDOException $e){
           echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    public function __destruct() {
        $this->conn = null;
        echo "Database is disconnected...\n";
    }
    
    public function getConnection() {
        return $this->conn;
    }
}
?>