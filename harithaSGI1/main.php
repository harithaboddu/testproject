<?php
include('DatabaseConnection.php');
include('PeopleDataTable.php');

$db_conn = new DatabaseConnection();
$conn = $db_conn->getConnection();

$table = new PeopleDataTable($conn);
$table->loadData();

$answer = -1;
$result = -1;
for ($year = 1900; $year <= 2000; $year++) {
    $count = $table->getAlivePeopleCountInYear($year);
    if ($count > $result)
    {
        $result = $count;
        $answer = $year;
    }
}

echo "Answer: In the year ".$answer.", most number of people (".$result." out of ".$table->getPeopleCount().") were alive.\n";

$table = null;
$db_conn = null;
?>
