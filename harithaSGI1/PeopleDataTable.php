<?php
class PeopleDataTable {
    private $conn;
    
    public function __construct(PDO $conn) {
        $this->conn = $conn;
        $this->create();
    }
    
    public function __destruct() {
        $this->drop();
    }
    
    private function create(){
        $sql = "CREATE TABLE IF NOT EXISTS PeopleData (
            ssn INT(20) PRIMARY KEY,
            birth_year SMALLINT unsigned NOT NULL,
            death_year SMALLINT unsigned NOT NULL
            )";
        try {
            $this->conn->exec($sql);
        }catch(PDOException $e){
           echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    private function drop(){
        $sql = 'DROP TABLE IF EXISTS `PeopleData`';
        try {
            $this->conn->exec($sql);
        }catch(PDOException $e){
          echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }

    public function loadData() {
        try {
            $sql = "LOAD DATA INFILE '../root/peopleData.csv' INTO TABLE PeopleData FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;";
            $this->conn->exec($sql);
            echo "People data loaded successfully...\n";
        }catch(PDOException $e){
            echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    public function getAlivePeopleCountInYear($year) {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM PeopleData WHERE birth_year <= $year AND death_year >= $year");
            $stmt->execute();
            return $stmt->rowCount();
        }
        catch(PDOException $e) {
            echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
    
    public function getPeopleCount() {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM PeopleData");
            $stmt->execute();
            return $stmt->rowCount();
        }
        catch(PDOException $e) {
            echo "EXCEPTION: ".$e->getMessage()."\n";
        }
    }
}
?>